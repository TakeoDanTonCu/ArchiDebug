class HTMLFinder:
    def __init__(self, path, dest):
        self.main(path, dest)

    def main(self, path, dest):
        #init
        writing = False
        filter = ["<ul>", "<p>", "</p>", "<li>", "</li>", "touch "]
        limit = {'start' : 'Itmustcontainthefollowingfilesanddirectories:', 'end' : 'Themkdir AUTHORSfile'}
        replacements = [ ("\n", ""), (" ", ""), ("</li>", ""), ("<b>", ""), ("</b>", ""), ("</code>", ""), ('<codeclass="s">', 'mkdir '), ("</ul>", "cd .."), ("<li>", "touch "), ('touch mkdir', 'mkdir')]
        data = open(dest, "w")
        text = open(path, "r")

        for lines in text:
            line = lines
            for tuples in replacements:
                line = line.replace(tuples[0], tuples[1])
            if line == limit["end"]:
                break
            if writing and line not in filter:
                if line != "":
                    data.write(line + "\n")
                if line[0:5] == "mkdir":
                    data.write("cd {}\n".format(line[6:]))

            if line == limit["start"]:
                writing = True

        data.close()
        text.close()


a = HTMLFinder("index.html", "data")
